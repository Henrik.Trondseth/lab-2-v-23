package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        ///// Oppgave 6
        pokemon1 = new Pokemon("Oddish", 100, 3);
        pokemon2 = new Pokemon("Pikachu", 94, 12);
        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString());
        for (int i = 0; i < 1000; i++) { 
            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
            if(!pokemon1.isAlive() || !pokemon2.isAlive())
                break;
        }
    }
}
