package INF101.lab2.pokemon;

import java.util.Random;

public class Pokemon {

    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;

    public Pokemon(String name, int healthPoints, int strength) {
        this.name = name;
        this.maxHealthPoints = healthPoints; 
        this.healthPoints = healthPoints; 
        this.strength = strength;
    }

    String getName() {
        return name;
    }

    int getStrength() {
        return strength;
    }

    int getCurrentHP() {
        return healthPoints;
    }
    
    int getMaxHP() {
        return maxHealthPoints;
    }

    boolean isAlive() {
        return healthPoints > 0;
    }

    void damage(int damageTaken) {
        if (damageTaken < 0) {
            System.out.println("Damage cannot be negative.");
            return;
        }
        healthPoints -= Math.min(damageTaken, healthPoints); 
        System.out.println(name + " takes " +  damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");
    }

    void attack(Pokemon target) {
        Random rand = new Random();
        int damageInflicted = rand.nextInt(this.strength) + 1;
        System.out.println(this.name + " attacks " + target.getName() + " with " + damageInflicted + " damage.");

        target.damage(damageInflicted);
        if (!target.isAlive()) {
            System.out.println(target.getName() + " is defeated by " + this.name + ".");
        }
    }

    @Override
    public String toString() {
        return String.format("%s HP: (%d/%d) STR: %d", name, healthPoints, maxHealthPoints, strength);
    }
}
